package az.ingress.ms24_esas.repo;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "hello")
@Data
public class HelloEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String message;
}
