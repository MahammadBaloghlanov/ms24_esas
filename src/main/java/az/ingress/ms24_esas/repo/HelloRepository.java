package az.ingress.ms24_esas.repo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface HelloRepository extends JpaRepository<HelloEntity,Long> {
}
