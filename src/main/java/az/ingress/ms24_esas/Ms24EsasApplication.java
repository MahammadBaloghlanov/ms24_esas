package az.ingress.ms24_esas;

import az.ingress.ms24_esas.repo.HelloEntity;
import az.ingress.ms24_esas.repo.HelloRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
@RequiredArgsConstructor
public class Ms24EsasApplication implements CommandLineRunner {

	private final HelloRepository repository;
	public static void main(String[] args) {
		SpringApplication.run(Ms24EsasApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

	}
}
