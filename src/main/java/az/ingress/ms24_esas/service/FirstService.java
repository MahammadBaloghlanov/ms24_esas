package az.ingress.ms24_esas.service;

import az.ingress.ms24_esas.repo.HelloEntity;
import az.ingress.ms24_esas.repo.HelloRepository;

public interface FirstService {

    HelloEntity sayHello();
}
