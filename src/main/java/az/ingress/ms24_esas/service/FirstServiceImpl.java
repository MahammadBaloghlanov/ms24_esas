package az.ingress.ms24_esas.service;

import az.ingress.ms24_esas.repo.HelloEntity;
import az.ingress.ms24_esas.repo.HelloRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
@Slf4j
@Service
@RequiredArgsConstructor
public class FirstServiceImpl implements FirstService{

    private final HelloRepository repository;
    @Override
    public HelloEntity sayHello() {
        log.info("Application startup completed");
        HelloEntity helloEntity = new HelloEntity();
//        helloEntity.setId(1L);
        helloEntity.setMessage("HelloWorld from db");
        repository.save(helloEntity);
        return helloEntity;
    }
}
