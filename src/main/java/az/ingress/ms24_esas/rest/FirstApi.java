package az.ingress.ms24_esas.rest;

import az.ingress.ms24_esas.repo.HelloEntity;
import az.ingress.ms24_esas.service.FirstService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ms24")
@RequiredArgsConstructor
public class FirstApi{

    private final FirstService service;

    @GetMapping("/hello1")
    public HelloEntity sayHello(){
       return service.sayHello();
    }

    @GetMapping("/hello2")
    public String sayHello1(){
        return "Hello world2";
    }

}
